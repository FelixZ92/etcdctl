FROM gliderlabs/alpine:3.1

ENV VERSION 3.2.10
ENV ETCDCTL_API 3

RUN apk-install curl && \
    curl -LOks https://github.com/coreos/etcd/releases/download/v${VERSION}/etcd-v${VERSION}-linux-amd64.tar.gz && \
    tar zxvf etcd-v${VERSION}-linux-amd64.tar.gz && \
    cp etcd-v${VERSION}-linux-amd64/etcdctl /etcdctl && \
    rm -rf etcd-v* && \
    chmod +x /etcdctl


